# CCNA3

## OSPF Open Shortest Path First

### Packet

Hello packet
Database description packet - DDP
Link-state request packet - LSR
Link-state update packet - LSU (LSA)
Link-state acknowledgment packet - LSAck

### Data Structures

**Adjacency database** - This creates the neighbor table.

- List of all neighbor routers to which a router has established bidirectional communication.
- This table is unique for each router.
- Can be viewed using the `show ip ospf neighbor` command.

**Link-state database (LSDB)** - This creates the topology table.

- Lists information about all other routers in the network.
- This database represents the network topology.
- All routers within an area have identical LSDB.
- Can be viewed using the `show ip ospf database` command.

**Forwarding database** - This creates the routing table.

- List of routes generated when an algorithm is run on the link-state database.
- The routing table of each router is unique and contains information on how and where to send packets to other routers.
- Can be viewed using `the show ip route` command.

### Algorithm

The router builds the topology table using results of calculations **based on the Dijkstra shortest-path first (SPF) algorithm**. The SPF algorithm is based on the cumulative cost to reach a destination.

The SPF algorithm creates an SPF tree by placing each router at the root of the tree and calculating the shortest path to each node. The SPF tree is then used to calculate the best routes. OSPF places the best routes into the forwarding database, which is used to make the routing table.

## Link-State Operation

To maintain routing information, OSPF routers complete a generic link-state routing process to reach a state of convergence. 

Link-state routing steps:

- Establish Neighbor Adjacencies
  
    OSPF-enabled routers must recognize each other on the network before they can share information. An OSPF-enabled router sends Hello packets out all OSPF-enabled interfaces to determine if neighbors are present on those links. If a neighbor is present, the OSPF-enabled router attempts to establish a neighbor adjacency with that neighbor.

- Exchange Link-State Advertisements

    After adjacencies are established, routers then exchange link-state advertisements (LSAs). LSAs contain the state and cost of each directly connected link. Routers flood their LSAs to adjacent neighbors. Adjacent neighbors receiving the LSA immediately flood the LSA to other directly connected neighbors, until all routers in the area have all LSAs.

- Build the Link State Database

    After LSAs are received, OSPF-enabled routers build the topology table (LSDB) based on the received LSAs. This database eventually holds all the information about the topology of the area.

- Execute the SPF Algorithm

    Routers then execute the SPF algorithm. The gears in the figure for this step are used to indicate the execution of the SPF algorithm. The SPF algorithm creates the SPF tree.

- Choose the Best Route

    After the SPF tree is built, the best paths to each network are offered to the IP routing table. The route will be inserted into the routing table unless there is a route source to the same network with a lower administrative distance, such as a static route. Routing decisions are made based on the entries in the routing table.

## Single-Area and Multiarea OSPF

**Single-Area OSPF** - All routers are in one area. Best practice is to use area 0.

**Multiarea OSPF** - OSPF is implemented using multiple areas, in a hierarchical fashion. All areas must connect to the backbone area (area 0). Routers interconnecting the areas are referred to as Area Border Routers (ABRs).

### Multiarea OSPF

Any time a router receives new information about a topology change within the area (including the addition, deletion, or modification of a link) the router must rerun the SPF algorithm, create a new SPF tree, and update the routing table.

**Note:** Routers in other areas receive updates regarding topology changes, but these routers only update the routing table, not rerun the SPF algorithm.

#### The hierarchical-topology design options with multiarea OSPF can offer the following advantages.

**Smaller routing tables** - Tables are smaller because there are fewer routing table entries. This is because network addresses can be summarized between areas. Route summarization is not enabled by default.

**Reduced link-state update overhead** - Designing multiarea OSPF with smaller areas minimizes processing and memory requirements.

**Reduced frequency of SPF calculations** - Multiarea OSPF localize the impact of a topology change within an area. For instance, it minimizes routing update impact because LSA flooding stops at the area boundary.

### OSPFv3

OSPFv3 is the OSPFv2 equivalent for exchanging IPv6 prefixes.

Similar to its IPv4 counterpart, OSPFv3 exchanges routing information to populate the IPv6 routing table with remote prefixes.

    OSPFv3 includes support for both IPv4 and IPv6.

OSPFv3 also uses the SPF algorithm as the computation engine to determine the best paths throughout the routing domain.

## Types of OSPF Packets

| Number | Name                                 | Description                                           |
|--------|--------------------------------------|-------------------------------------------------------|
| 1      | Hello                                | Discovers neighbors and builds adjacencies between them |
| 2      | Database Description (DBD)           | Checks for database synchronization between routers  |
| 3      | Link-State Request (LSR)             | Requests specific link-state records from router to router |
| 4      | Link-State Update (LSU)              | Sends specifically requested link-state records      |
| 5      | Link-State Acknowledgment (LSAck)    | Acknowledges the other packet types                   |

## Link-State Updates

### Hello Packet

![Alt text](image.png)

### Type 2 - DBD

Routers initially exchange Type 2 DBD packets, which is an abbreviated list of the LSDB of the sending router. It is used by receiving routers to check against the local LSDB.

### Type 3 - LSR

A Type 3 LSR packet is used by the receiving routers to request more information about an entry in the DBD.

### Type 4 - LSU

The Type 4 LSU packet is used to reply to an LSR packet. LSUs are also used to forward OSPF routing updates, such as link changes.

### Type 5 - LSAck

A Type 5 packet is used to acknowledge the receipt of a Type 4 LSU.

![Alt text](image-1.png)





